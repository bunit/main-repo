﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Saif Alireza
public class NoDestroyMusic : MonoBehaviour {

	static NoDestroyMusic instance = null;

	void Awake() {
		DontDestroyOnLoad (transform.gameObject);

		if (instance != null)
		{
			Destroy(gameObject);
		}
		else { instance = this;
			DontDestroyOnLoad(gameObject);
		}
	}
}
