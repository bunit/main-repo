﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//Saif Alireza
public class ButtonManager : MonoBehaviour {

	public void ButtonPlay(string playLevel) {
		Initiate.Fade("Level_1",Color.black,1f);
	}

	public void ButtonOptions(string optionsLevel) {
		Initiate.Fade("OptionsMenu",Color.black,1f);
	}

	public void ButtonCredits(string creditsLevel) {
		Initiate.Fade("Credits",Color.black,1f);
	}

	public void ButtonExit() {
		Application.Quit ();
	}

	public void ButtonReturn(string returnLevel) {
		Initiate.Fade("MainMenu",Color.black,1f);
	}

	public void ButtonRetry(string retryLevel) {
		Initiate.Fade("Level_1",Color.black,1f);
	}

	public void ButtonMenu(string menuLevel) {
		Initiate.Fade("MainMenu",Color.black,1f);
	}
}
