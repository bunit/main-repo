﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Saif Alireza
public class AudioManager : MonoBehaviour {
	
	public enum AudioChannel {Master, Sfx, Music};

	public float masterVolumePercent { get; private set; }
	public float sfxVolumePercent { get; private set; }
	public float musicVolumePercent { get; private set; }

	AudioSource[] musicSources;
	int activeMusicScourceIndex;

	public static AudioManager instance;

	Transform audioListener;

	SoundLibrary library;

	void Awake() {

		if (instance != null) {
			Destroy (gameObject);
		} else {

			instance = this;
			DontDestroyOnLoad (gameObject);

			library = GetComponent<SoundLibrary> ();

			musicSources = new AudioSource[2];
			for (int i = 0; i < 2; i++) {
				GameObject newMusicSource = new GameObject ("Music source " + (i + 1));
				musicSources [i] = newMusicSource.AddComponent<AudioSource> ();
				newMusicSource.transform.parent = transform;
			}

			audioListener = FindObjectOfType<AudioListener> ().transform;

			//masterVolumePercent = PlayerPrefs.SetFloat ("master vol", 1);
			//sfxVolumePercent = PlayerPrefs.SetFloat ("sfx vol", 1);
			//musicVolumePercent = PlayerPrefs.SetFloat ("music vol", 1);

		}
	}

	public void SetVolume(float volumePercent, AudioChannel channel) {
		switch (channel) {
		case AudioChannel.Master:
			masterVolumePercent = volumePercent;
			break;
		case AudioChannel.Sfx:
			sfxVolumePercent = volumePercent;
			break;
		case AudioChannel.Music:
			musicVolumePercent = volumePercent;
				break;
		}

		musicSources [0].volume = musicVolumePercent * masterVolumePercent;
		musicSources [1].volume = musicVolumePercent * masterVolumePercent;

		PlayerPrefs.SetFloat ("master vol", masterVolumePercent);
		PlayerPrefs.SetFloat ("sfx vol", sfxVolumePercent);
		PlayerPrefs.SetFloat ("music vol", musicVolumePercent);
		PlayerPrefs.Save ();
	}

	public void PlayMusic(AudioClip clip, float fadeDuration = 1) {
		activeMusicScourceIndex = 1 - activeMusicScourceIndex;
		musicSources [activeMusicScourceIndex].clip = clip;
		musicSources [activeMusicScourceIndex].Play ();

		StartCoroutine (AnimateMusicCrossfade (fadeDuration));
	}

	public void PlaySound(AudioClip clip, Vector3 pos) {
		if (clip != null) {
			AudioSource.PlayClipAtPoint (clip, pos, sfxVolumePercent * masterVolumePercent);
		}
	}

	public void PlaySound(string soundName, Vector3 pos) {
		PlaySound (library.GetClipFromName (soundName), pos);
	}

	IEnumerator AnimateMusicCrossfade(float duration) {
		float percent = 0;

		while (percent < 1) {
			percent += Time.deltaTime * 1 / duration;
			musicSources [activeMusicScourceIndex].volume = Mathf.Lerp (0, musicVolumePercent * masterVolumePercent, percent);
			musicSources [1 - activeMusicScourceIndex].volume = Mathf.Lerp (musicVolumePercent * masterVolumePercent, 0, percent);
			yield return null;
		}
	}
}