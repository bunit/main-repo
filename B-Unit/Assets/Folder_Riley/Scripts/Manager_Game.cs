﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//The Entirety of this script is the work of Riley Shanley. Recorded 12/8/2017

public class Tile
{
    public GameObject tileObj;
    public string type;
    public Tile(GameObject obj, string t)
    {
        tileObj = obj;
        type = t;
    }
}

public class Manager_Game : MonoBehaviour
{
    public GameObject timercolor;
    bool mademove = false;
    public GameObject sparkle;
    public AudioClip[] Sounds;
    public bool ValidMove;
    public float GameTimer = 50;
    public Score Score;
    float tempScore;
    GameObject tile1 = null;
    GameObject tile2 = null;
    bool renewBoard = false;
    public GameObject[] tile;
    List<GameObject> tileBank = new List<GameObject>();
    public Text ScoreText;
    public Text LevelText;
    public GameObject TimerBar;
    public AudioClip[] setsounds;
    bool timefreeze;
    float freezetimer;
    int counter = 1;
    int matches = 1;

    static int rows = 9;
    static int columns = 8;
    Tile[,] tiles = new Tile[columns, rows];


    void ShuffleList()
    {
        System.Random rand = new System.Random();
        int r = tileBank.Count;
        while (r > 1)
        {
            r--;
            int n = rand.Next(r + 1);
            GameObject val = tileBank[n];
            tileBank[n] = tileBank[r];
            tileBank[r] = val;
        }
    }


    void Start()
    {
        Score = FindObjectOfType<Score>();
        tempScore = Score.GameScore;
        if(Score.LevelCount > 1)
        {
            GetComponent<AudioSource>().PlayOneShot(setsounds[0]);
        }
        int numCopies = (rows * columns) / 3;
        for (int i = 0; i < numCopies; i++)
        {
            for (int j = 0; j < tile.Length; j++)
            {
                GameObject o = (GameObject)Instantiate(tile[j], new Vector3(-10, -10, 0), tile[j].transform.rotation);
                o.SetActive(false);
                tileBank.Add(o);
            }
        }

        ShuffleList();

        //Initialize Grid

        for (int r = 0; r < rows; r++)
        {
            for (int c = 0; c < columns; c++)
            {
                Vector3 tilePos = new Vector3(c, r, 0);
                for (int n = 0; n < tileBank.Count; n++)
                {
                    GameObject o = tileBank[n];
                    if (!o.activeSelf)
                    {
                        o.transform.position = new Vector3(tilePos.x, tilePos.y, tilePos.z);
                        o.SetActive(true);
                        tiles[c, r] = new Tile(o, o.name);
                        n = tileBank.Count + 1;
                    }
                }
            }
        }
    }

    void Update()
    {
        //run the timer
        LevelText.text = "Level: " + Score.LevelCount + "";
        if (timefreeze == false)
        {
            Timer();
        }     
        if(timefreeze == true && mademove == true)
        {
            timercolor.GetComponent<Image>().color = Color.blue;
            freezetimer += Time.deltaTime;
            if(freezetimer >= 3)
            {
                timefreeze = false;
                freezetimer = 0;
            }
        }
        else
        {
            timercolor.GetComponent<Image>().color = Color.yellow;
        }
        if (GameTimer < 0)
        {
            if(GetComponent<AudioSource>().isPlaying != true)
            {
                GetComponent<AudioSource>().PlayOneShot(setsounds[1]);
            }            
            Initiate.Fade("GameOverTime", Color.black, 1f);
            Destroy(Score.gameObject);
        }
        if (GameTimer >= 100)
        {
            GameTimer = 50;
            Score.DifficultyMod += .5f;
            Score.LevelCount++;
            DontDestroyOnLoad(Score);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        //do not award points before player has moved
        if (mademove == false)
        {
            if (tempScore != Score.GameScore)
            {
                Score.GameScore = tempScore;
            }
        }
        ScoreText.text = "" + Score.GameScore + "";
        //Grab First Tile
        CheckGrid();

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.GetRayIntersection(ray, 1000);
            if (hit)
            {
                tile1 = hit.collider.gameObject;
            }
        }
        //Grab Second Tile
        else if (Input.GetMouseButtonUp(0) && tile1)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.GetRayIntersection(ray, 1000);
            if (hit)
            {
                tile2 = hit.collider.gameObject;
            }


            if (tile1 && tile2)
            {
                int horDist = (int)Mathf.Abs(tile1.transform.position.x - tile2.transform.position.x);
                int vertDist = (int)Mathf.Abs(tile1.transform.position.y - tile2.transform.position.y);
                ValidMove = false;
                if (horDist == 1 & vertDist == 0 || horDist == 0 & vertDist == 1)
                {
                    //Adjust Tile Position Within The Array Database
                    
                    Vector3 temPos = tile1.transform.position;
                    Tile temp = tiles[(int)tile1.transform.position.x, (int)tile1.transform.position.y];
                    tiles[(int)tile1.transform.position.x, (int)tile1.transform.position.y] = tiles[(int)tile2.transform.position.x, (int)tile2.transform.position.y];
                    tiles[(int)tile2.transform.position.x, (int)tile2.transform.position.y] = temp;
                    tile1.transform.position = tile2.transform.position;
                    tile2.transform.position = temPos;
                    CheckGrid();                   
                    if (ValidMove == true)
                    {                       
                        Debug.Log("Valid Move");
                        ValidMove = false;
                        tile1 = null;
                        tile2 = null;
                        mademove = true;
                    }
                    else if (ValidMove == false)
                    {
                        tile2.transform.position = tile1.transform.position;
                        tile1.transform.position = temPos;
                        tiles[(int)tile2.transform.position.x, (int)tile2.transform.position.y] = tiles[(int)tile1.transform.position.x, (int)tile1.transform.position.y];
                        tiles[(int)tile1.transform.position.x, (int)tile1.transform.position.y] = temp;
                        Debug.Log("Invalid Move");
                        ValidMove = false;
                        tile1 = null;
                        tile2 = null;
                        GetComponent<AudioSource>().PlayOneShot(setsounds[2]);
                    }
                }
            }
        }
    }
    public void CheckGrid()
    {
        //check in columns
        for (int r = 0; r < rows; r++)
        {
            counter = 1;
            matches = 1;
            for (int c = 1; c < columns; c++)
            {
                if (tiles[c, r] != null && tiles[c - 1, r] != null)
                //if the tiles exist
                {
                    if (tiles[c, r].type == tiles[c - 1, r].type)
                    {
                        counter++;
                        matches++;
                        if(c == 7)
                        {
                            //Debug.Log("Counter: " + counter + " Matches: " + matches + " Column: " + c + " Row: " + r + "");
                            if(matches == 3)
                            {
                                Score.GameScore += 30;
                                if (mademove == true)
                                {
                                    GameTimer += 5;
                                }
                                Debug.Log("Matched 3, On Border, Columns");
                                if (tiles[c , r] != null)
                                    tiles[c , r].tileObj.SetActive(false);
                                if (tiles[c - 1, r] != null)
                                    tiles[c - 1, r].tileObj.SetActive(false);
                                if (tiles[c - 2, r] != null)
                                    tiles[c - 2, r].tileObj.SetActive(false);
                                /*for (int s = 0; s < 2; s++)
                                {
                                    var NewSparkle = Instantiate(sparkle);
                                    NewSparkle.transform.position = tiles[c-s, r].tileObj.transform.position;
                                    
                                }*/
                                tiles[c , r] = null;
                                tiles[c - 1, r] = null;
                                tiles[c - 2, r] = null;
                                renewBoard = true;
                                GetComponent<AudioSource>().PlayOneShot(Sounds[Random.Range(0,3)]);
                                matches = 1;
                                ValidMove = true;
                            }
                            else if (matches == 4)
                            {
                                Score.GameScore += 60;
                                if (mademove == true)
                                {
                                    GameTimer += 8;
                                }
                                Debug.Log("Matched 4, On Border, Columns");
                                if (tiles[c, r] != null)
                                    tiles[c, r].tileObj.SetActive(false);
                                if (tiles[c - 1, r] != null)
                                    tiles[c - 1, r].tileObj.SetActive(false);
                                if (tiles[c - 2, r] != null)
                                    tiles[c - 2, r].tileObj.SetActive(false);
                                if (tiles[c - 3, r] != null)
                                    tiles[c - 3, r].tileObj.SetActive(false);
                                /*for (int s = 0; s < 3; s++)
                                {
                                    var NewSparkle = Instantiate(sparkle);
                                    NewSparkle.transform.position = tiles[c - s, r].tileObj.transform.position;
                                    
                                }*/
                                tiles[c, r] = null;
                                tiles[c - 1, r] = null;
                                tiles[c - 2, r] = null;
                                tiles[c - 3, r] = null;
                                renewBoard = true;
                                GetComponent<AudioSource>().PlayOneShot(Sounds[Random.Range(0, 3)]);
                                matches = 1;
                                ValidMove = true;
                                timefreeze = true;
                                freezetimer = 1;
                            }
                            else if(matches == 5)
                            {
                                Score.GameScore += 100;
                                if (mademove == true)
                                {
                                    GameTimer += 12;
                                }
                                Debug.Log("Matched 5, On Border, Columns");
                                if (tiles[c, r] != null)
                                    tiles[c, r].tileObj.SetActive(false);
                                if (tiles[c - 1, r] != null)
                                    tiles[c - 1, r].tileObj.SetActive(false);
                                if (tiles[c - 2, r] != null)
                                    tiles[c - 2, r].tileObj.SetActive(false);
                                if (tiles[c - 3, r] != null)
                                    tiles[c - 3, r].tileObj.SetActive(false);
                                if (tiles[c - 4, r] != null)
                                    tiles[c - 4, r].tileObj.SetActive(false);
                               /* for (int s = 0; s < 4; s++)
                                {
                                    var NewSparkle = Instantiate(sparkle);
                                    NewSparkle.transform.position = tiles[c - s, r].tileObj.transform.position;
                                }*/
                                tiles[c, r] = null;
                                tiles[c - 1, r] = null;
                                tiles[c - 2, r] = null;
                                tiles[c - 3, r] = null;
                                tiles[c - 4, r] = null;
                                renewBoard = true;
                                GetComponent<AudioSource>().PlayOneShot(Sounds[Random.Range(0, 3)]);
                                matches = 1;
                                ValidMove = true;
                                timefreeze = true;
                                freezetimer = 0;
                            }
                        }
                    }
                    else
                        counter = 1;
                    if (counter == 1)
                    {
                        if (matches == 3)
                        {
                            Score.GameScore += 30;
                            if (mademove == true)
                            {
                                GameTimer += 5;
                            }
                            Debug.Log("Matched 3, Not On Border, Columns");
                            if (tiles[c - 1, r] != null)
                                tiles[c - 1, r].tileObj.SetActive(false);
                            if (tiles[c - 2, r] != null)
                                tiles[c - 2, r].tileObj.SetActive(false);
                            if (tiles[c - 3, r] != null)
                                tiles[c - 3, r].tileObj.SetActive(false);
                            /*for (int s = 1; s < 3; s++)
                            {
                                var NewSparkle = Instantiate(sparkle);
                                NewSparkle.transform.position = tiles[c - s, r].tileObj.transform.position;
                            }*/
                            tiles[c - 1, r] = null;
                            tiles[c - 2, r] = null;
                            tiles[c - 3, r] = null;
                            renewBoard = true;
                            GetComponent<AudioSource>().PlayOneShot(Sounds[Random.Range(0, 3)]);
                            matches = 1;
                            ValidMove = true;
                        }
                        else if (matches == 4)
                        {
                            Score.GameScore += 60;
                            if (mademove == true)
                            {
                                GameTimer += 8;
                            }
                            Debug.Log("Matched 4, Not On Border, Columns");
                            if (tiles[c - 1, r] != null)
                                tiles[c - 1, r].tileObj.SetActive(false);
                            if (tiles[c - 2, r] != null)
                                tiles[c - 2, r].tileObj.SetActive(false);
                            if (tiles[c - 3, r] != null)
                                tiles[c - 3, r].tileObj.SetActive(false);
                            if (tiles[c - 4, r] != null)
                                tiles[c - 4, r].tileObj.SetActive(false);
                            /*for (int s = 1; s < 4; s++)
                            {
                                var NewSparkle = Instantiate(sparkle);
                                NewSparkle.transform.position = tiles[c - s, r].tileObj.transform.position;
                            }*/
                            tiles[c - 1, r] = null;
                            tiles[c - 2, r] = null;
                            tiles[c - 3, r] = null;
                            tiles[c - 4, r] = null;
                            renewBoard = true;
                            GetComponent<AudioSource>().PlayOneShot(Sounds[Random.Range(0, 3)]);
                            matches = 1;
                            ValidMove = true;
                            timefreeze = true;
                            freezetimer = 1;
                        }
                        else if (matches == 5)
                        {
                            Score.GameScore += 100;
                            if (mademove == true)
                            {
                                GameTimer += 12;
                            }
                            Debug.Log("Matched 5, Not On Border, Columns");
                            if (tiles[c - 1, r] != null)
                                tiles[c - 1, r].tileObj.SetActive(false);
                            if (tiles[c - 2, r] != null)
                                tiles[c - 2, r].tileObj.SetActive(false);
                            if (tiles[c - 3, r] != null)
                                tiles[c - 3, r].tileObj.SetActive(false);
                            if (tiles[c - 4, r] != null)
                                tiles[c - 4, r].tileObj.SetActive(false);
                            if (tiles[c - 5, r] != null)
                                tiles[c - 5, r].tileObj.SetActive(false);
                            timefreeze = true;
                            /*for (int s = 1; s < 5; s++)
                            {
                                var NewSparkle = Instantiate(sparkle);
                                NewSparkle.transform.position = tiles[c - s, r].tileObj.transform.position;
                            }*/
                            tiles[c - 1, r] = null;
                            tiles[c - 2, r] = null;
                            tiles[c - 3, r] = null;
                            tiles[c - 4, r] = null;
                            tiles[c - 5, r] = null;

                            renewBoard = true;
                            GetComponent<AudioSource>().PlayOneShot(Sounds[Random.Range(0, 3)]);
                            matches = 1;
                            ValidMove = true;
                            timefreeze = true;
                            freezetimer = 0;
                        }
                        else
                        {
                            matches = 1;
                        }
                    }                    
                }
            }
        }
        //check in rows
        for (int c = 0; c < columns; c++)
        {
            counter = 1;
            matches = 1;
            for (int r = 1; r < rows; r++)
            {
                if (tiles[c, r] != null && tiles[c, r - 1] != null)
                //if tiles exist
                {
                    if (tiles[c, r].type == tiles[c, r - 1].type)
                    {
                        counter++;
                        matches++;
                        if (r == 8)
                        {
                            //Debug.Log("Counter: " + counter + " Matches: " + matches + " Column: " + c + " Row: " + r + "");
                            if (matches == 3)
                            {
                                Debug.Log("Matched 3, On Border, Rows");
                                Score.GameScore += 30;
                                if (mademove == true)
                                {
                                    GameTimer += 5;
                                }
                                if (tiles[c, r] != null)
                                    tiles[c, r].tileObj.SetActive(false);
                                if (tiles[c , r-1] != null)
                                    tiles[c , r-1].tileObj.SetActive(false);
                                if (tiles[c , r-2] != null)
                                    tiles[c , r-2].tileObj.SetActive(false);
                                /*for (int s = 1; s < 2; s++)
                                {
                                    var NewSparkle = Instantiate(sparkle);
                                    NewSparkle.transform.position = tiles[c, r - s].tileObj.transform.position;
                                }*/
                                tiles[c, r] = null;
                                tiles[c , r-1] = null;
                                tiles[c , r-2] = null;
                                renewBoard = true;
                                GetComponent<AudioSource>().PlayOneShot(Sounds[Random.Range(0, 3)]);
                                matches = 1;
                                ValidMove = true;
                            }
                            else if (matches == 4)
                            {
                                Score.GameScore += 60;
                                if (mademove == true)
                                {
                                    GameTimer += 8;
                                }
                                Debug.Log("Matched 4, On Border, Rows");
                                if (tiles[c, r] != null)
                                    tiles[c, r].tileObj.SetActive(false);
                                if (tiles[c , r-1] != null)
                                    tiles[c , r-1].tileObj.SetActive(false);
                                if (tiles[c , r-2] != null)
                                    tiles[c , r-2].tileObj.SetActive(false);
                                if (tiles[c , r-3] != null)
                                    tiles[c , r-3].tileObj.SetActive(false);
                                /*for (int s = 1; s < 3; s++)
                                {
                                    var NewSparkle = Instantiate(sparkle);
                                    NewSparkle.transform.position = tiles[c, r - s].tileObj.transform.position;
                                }*/
                                tiles[c, r] = null;
                                tiles[c , r-1] = null;
                                tiles[c , r-2] = null;
                                tiles[c , r-3] = null;
                                renewBoard = true;
                                GetComponent<AudioSource>().PlayOneShot(Sounds[Random.Range(0, 3)]);
                                matches = 1;
                                ValidMove = true;
                                timefreeze = true;
                                freezetimer = 1;
                            }
                            else if (matches == 5)
                            {
                                Score.GameScore += 100;
                                if (mademove == true)
                                {
                                    GameTimer += 12;
                                }
                                Debug.Log("Matched 5, On Border, Rows");
                                if (tiles[c, r] != null)
                                    tiles[c, r].tileObj.SetActive(false);
                                if (tiles[c , r-1] != null)
                                    tiles[c , r-1].tileObj.SetActive(false);
                                if (tiles[c , r-2] != null)
                                    tiles[c , r-2].tileObj.SetActive(false);
                                if (tiles[c , r-3] != null)
                                    tiles[c , r-3].tileObj.SetActive(false);
                                if (tiles[c , r-4] != null)
                                    tiles[c , r-4].tileObj.SetActive(false);
                                for (int s = 0; s < 4; s++)
                                /*{
                                    var NewSparkle = Instantiate(sparkle);
                                    NewSparkle.transform.position = tiles[c, r - s].tileObj.transform.position;
                                }*/
                                tiles[c, r] = null;
                                tiles[c , r-1] = null;
                                tiles[c , r-2] = null;
                                tiles[c , r-3] = null;
                                tiles[c , r-4] = null;
                                renewBoard = true;
                                GetComponent<AudioSource>().PlayOneShot(Sounds[Random.Range(0, 3)]);
                                matches = 1;
                                ValidMove = true;
                                timefreeze = true;
                                freezetimer = 0;
                            }
                        }
                    }
                    else
                        counter = 1;
                    if (counter == 1)
                    {
                        if (matches == 3)
                        {
                            Score.GameScore += 30;
                            if (mademove == true)
                            {
                                GameTimer += 5;
                            }
                            Debug.Log("Matched 3, Not On Border, Rows");
                            if (tiles[c, r - 1] != null)
                                tiles[c, r - 1].tileObj.SetActive(false);
                            if (tiles[c, r - 2] != null)
                                tiles[c, r - 2].tileObj.SetActive(false);
                            if (tiles[c, r - 3] != null)
                                tiles[c, r - 3].tileObj.SetActive(false);
                            tiles[c, r - 1] = null;
                            tiles[c, r - 2] = null;
                            tiles[c, r - 3] = null;
                            /*for (int s = 1; s < 3; s++)
                            {
                                var NewSparkle = Instantiate(sparkle);
                                NewSparkle.transform.position = tiles[c, r - s].tileObj.transform.position;                              
                            }*/
                            renewBoard = true;
                            GetComponent<AudioSource>().PlayOneShot(Sounds[Random.Range(0, 3)]);
                            matches = 1;
                            ValidMove = true;
                        }
                        else if (matches == 4)
                        {
                            Score.GameScore += 60;
                            if (mademove == true)
                            {
                                GameTimer += 8;
                            }
                            Debug.Log("Matched 4, Not On Border, Rows");
                            if (tiles[c, r - 1] != null)
                                tiles[c, r - 1].tileObj.SetActive(false);
                            if (tiles[c, r - 2] != null)
                                tiles[c, r - 2].tileObj.SetActive(false);
                            if (tiles[c, r - 3] != null)
                                tiles[c, r - 3].tileObj.SetActive(false);
                            if (tiles[c, r - 4] != null)
                                tiles[c, r - 4].tileObj.SetActive(false);
                            /*for (int s = 1; s < 4; s++)
                            {
                                var NewSparkle = Instantiate(sparkle);
                                NewSparkle.transform.position = tiles[c, r - s].tileObj.transform.position;
                            }*/
                            tiles[c, r - 1] = null;
                            tiles[c, r - 2] = null;
                            tiles[c, r - 3] = null;
                            tiles[c, r - 4] = null;
                            renewBoard = true;
                            GetComponent<AudioSource>().PlayOneShot(Sounds[Random.Range(0, 3)]);
                            matches = 1;
                            ValidMove = true;
                            timefreeze = true;
                            freezetimer = 1;
                        }
                        else if (matches == 5)
                        {
                            Score.GameScore += 100;
                            if (mademove == true)
                            {
                                GameTimer += 12;
                            }
                            Debug.Log("Matched 5, Not On Border, Rows");
                            if (tiles[c, r - 1] != null)
                                tiles[c, r - 1].tileObj.SetActive(false);
                            if (tiles[c, r - 2] != null)
                                tiles[c, r - 2].tileObj.SetActive(false);
                            if (tiles[c, r - 3] != null)
                                tiles[c, r - 3].tileObj.SetActive(false);
                            if (tiles[c, r - 4] != null)
                                tiles[c, r - 4].tileObj.SetActive(false);
                            if (tiles[c, r - 5] != null)
                                tiles[c, r - 5].tileObj.SetActive(false);
                            /*for (int s = 1; s < 5; s++)
                            {
                                var NewSparkle = Instantiate(sparkle);
                                NewSparkle.transform.position = tiles[c, r-s].tileObj.transform.position;
                            }*/
                            tiles[c, r - 1] = null;
                            tiles[c, r - 2] = null;
                            tiles[c, r - 3] = null;
                            tiles[c, r - 4] = null;
                            tiles[c, r - 5] = null;

                            renewBoard = true;
                            GetComponent<AudioSource>().PlayOneShot(Sounds[Random.Range(0, 3)]);
                            matches = 1;
                            ValidMove = true;
                            timefreeze = true;
                            freezetimer = 0;
                        }
                        else
                        {
                            matches = 1;
                        }                        
                    }
                }
            }
        }
        if (renewBoard)
        {
            RenewGrid();
            renewBoard = false;
        }
    }
    void RenewGrid()
    {
        bool anyMoved = false;
        ShuffleList();
        for (int r = 1; r < rows; r++)
        {
            for (int c = 0; c < columns; c++)
            {
                if (r == rows - 1 && tiles[c, r] == null)
                //if top row and empty
                {
                    Vector3 tilePos = new Vector3(c, r, 0);
                    for (int n = 0; n < tileBank.Count; n++)
                    {
                        GameObject o = tileBank[n];
                        if (!o.activeSelf)
                        {
                            o.transform.position = new Vector3(tilePos.x, tilePos.y, tilePos.z);
                            o.SetActive(true);
                            tiles[c, r] = new Tile(o, o.name);
                            n = tileBank.Count + 1;
                        }
                    }
                }
                if (tiles[c, r] != null)
                {
                    //drop down if space below is empty
                    if (tiles[c, r - 1] == null)
                    {
                        tiles[c, r - 1] = tiles[c, r];
                        tiles[c, r - 1].tileObj.transform.position = new Vector3(c, r - 1, 0);
                        tiles[c, r] = null;
                        anyMoved = true;
                    }
                }
            }
        }
        if (anyMoved)
        {
            Invoke("RenewGrid", .5f);
        }
    }
    void Timer()
    {
        GameTimer -= (Time.deltaTime + Score.DifficultyMod * .01f);
        // Debug.Log(GameTimer);
        TimerBar.GetComponent<Slider>().value = GameTimer;
    }
}
