﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The Entirety of this script is the work of Riley Shanley. Recorded 12/8/2017

public class Score : MonoBehaviour {
    public float GameScore;
    public float DifficultyMod;
    public float LevelCount;
    // Use this for initialization
    public static Score instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            LevelCount++;
        }
        else
        {
            Destroy(this.gameObject);
            return;
        }
    }
}
