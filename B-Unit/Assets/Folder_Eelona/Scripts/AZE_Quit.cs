﻿/*        

//        Developer Name: Eelona Allison

//         Contribution: Load Scene when button is clicked 

//                Options UI and Credits UI

//                Start Date 11/29/17 End Date 12/1/17

//                References: Unity Tutorials

//                        Links: https://unity3d.com/learn/tutorials/topics/user-interface-ui/creating-main-menu?playlist=17111

//*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AZE_Quit : MonoBehaviour {

	public void Quit(){
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.Quit ();
		#endif
	}
}
