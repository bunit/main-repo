﻿/*        

//        Developer Name: Eelona Allison

//         Contribution: Load Scene when button is clicked 

//                Options UI and Credits UI

//                Start Date 11/29/17 End Date 12/1/17

//                References: Unity Scrpting API

//                        Links: https://docs.unity3d.com/ScriptReference/SceneManagement.SceneManager.LoadScene.html

//*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AZE_LoadScene: MonoBehaviour {

	public void LoadByIndex (int scene){
		//SceneManager.LoadScene (scene);
		Initiate.Fade ("MainMenu", Color.black,1f);
	}
}
