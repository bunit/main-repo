﻿/*        

//        Developer Name: Eelona Allison

//         Contribution: Load Scene when button is clicked 

//                Options UI and Credits UI

//                Start Date 11/29/17 End Date 12/1/17

//                References: Unity Scrpting API, Pix Lab Games

//                        Links: https://docs.unity3d.com/ScriptReference/MonoBehaviour.OnMouseDown.html, https://www.youtube.com/watch?v=dvyiQLkaMcg&t=1210s

//*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AZE_Pause : MonoBehaviour {

	public GameObject pausePanel;


	public bool Paused;
	// Use this for initialization
	void Start () {
		Paused = false;
	}

	// Update is called once per frame
	void Update () {
		if(Paused){
			OnPause (true);
		}
		else{
			OnPause (false);
		}


	}

	public void OnMouseDown(){
		Pause ();
	}
	public void OnPause(bool state){
		if(state){
			
			Time.timeScale = 0.0f;
		}

		else{
			Time.timeScale = 1.0f;

		}
		pausePanel.SetActive (state);
	}

	public void Pause(){
		if(Paused){
			Paused = false;
		}

		else{
			Paused = true;
		}
	}
		
}
